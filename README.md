#Microsoft: Microsoft: DEV210x Introduction to C++

![edx-microsoft-logo.jpg](https://bitbucket.org/repo/9zoaE8/images/1239304987-edx-microsoft-logo.jpg)


#Module 0

Important Pre-Course Survey

About this Course

Introduce Yourself


#Module 1 

What Exactly is C++? 

2 of 2 possible points (2/2) 100%

Problem Scores:  1/1 1/1

Portability, Compiling, and Linking

2 of 2 possible points (2/2) 100%


Problem Scores:  1/1 1/1

C++ Fundamentals 

1 of 1 possible points (1/1) 100%


Problem Scores:  1/1


#Module 2 

Available Data Types 2 of 2 possible points (2/2) 100%


Problem Scores:  1/1 1/1

Variables and Constants 3 of 3 possible points (3/3) 100%

Problem Scores:  1/1 1/1 1/1

Complex Data Types 4 of 4 possible points (4/4) 100%


Problem Scores:  1/1 1/1 1/1 1/1


Homework


#Module 3
C++ Operators

Decision Statements 

2 of 2 possible points (2/2) 100%

Problem Scores:  1/1 1/1

Repetition Statements 

2 of 2 possible points (2/2) 100%

Problem Scores:  1/1 1/1

Peer Review 1 of 1 possible points (1/1) 100%

Homework

#Module 4
Introducing Functions 

3 of 3 possible points (3/3) 100%

Problem Scores:  1/1 1/1 1/1

Introducing Objects (Classes)

2 of 2 possible points (2/2) 100%

Problem Scores:  1/1 1/1

Encapsulation 1 of 1 possible points (1/1) 100%

Problem Scores:  1/1

Const Objects 1 of 1 possible points (1/1) 100%

Problem Scores:  1/1

Peer Review 0 of 1 possible points (0/1)

Homework

