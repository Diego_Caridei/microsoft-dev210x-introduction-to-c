//
//  main.cpp
//  Assignment_3b
//
//  Created by Guybrush_Treepwood on 05/10/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#include <iostream>
using namespace std;

float avg(int myarray[], int length) {
    float average=0;
    for (int i = 0; i < length; i++) {
        average += myarray[i];
    }
    return average / length;
    
}
int main(int argc, const char * argv[]) {
    int myarray[10] = { 1,2,3,4,5,6,7,8,9,10 };
    
    float average = avg(myarray, 10);
    
    cout << average << endl;
    return 0;
}
