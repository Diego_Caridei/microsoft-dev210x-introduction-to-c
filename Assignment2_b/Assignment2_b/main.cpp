//
//  main.cpp
//  Assignment2_b
//
//  Created by Guybrush_Treepwood on 05/10/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    // insert code here...
    char option = 'y';
    
    switch (option)
    {
        case 'Y':
        case 'y':
            std::cout << "You chose y or Y\n";
            break;
            
        case 'N':
        case 'n':
            std::cout << "You chose n or N\n";
            break;
            
        default:
            std::cout << "You didn't choose a valid option\n";
            break;
    }
    
    return 0;
}
