//
//  main.cpp
//  Assignment2_a
//
//  Created by Guybrush_Treepwood on 05/10/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[]) {
    // insert code here...
    int base = 2;
    int power = 8;
    
    int result = 1;
    
    for (int i = 0; i < power; i++)
    {
        result *= base;
    }
    
    std::cout << "The raised power of " << base << " to the " << power << " is: " << result << "\n";
    return 0;
}
