//
//  main.cpp
//  Assignment3_a
//
//  Created by Guybrush_Treepwood on 05/10/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#include <iostream>
using namespace std;

int pow(int base, unsigned int expo) {
    int res;
    if (expo == 0)
    {
        res = 1;
    }
    else
    {
        res = base;
        for (int i = 0; i<expo; i++)
        {
            res *= base;
        }
    }
    return res;
}

float sine(float opposite, float hypotenuse) {
    
    float sin = opposite / hypotenuse;
    
    return sin;
}
int main(int argc, const char * argv[]) {
    int base = 5;
    unsigned int expo = 3;
    
    float opposite = 4.;
    float hypotenuse = 5.;
    
    int power = pow(base, expo);
    float sin = sine(opposite, hypotenuse);
    
    cout << power << endl;
    cout << sin << endl;
    return 0;
}
